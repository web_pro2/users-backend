import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductsDto } from './dto/create-products.dto';
import { UpdateProductsDto } from './dto/update-products.dto';
import { Products } from './entities/products.entity';

let products: Products[] = [
  { id: 1, name: 'cake', price: 60 },
  { id: 2, name: 'candy', price: 10 },
  { id: 3, name: 'bread', price: 40 },
];
let lastProductsID = 4;

@Injectable()
export class ProductsService {
  create(createProductsDto: CreateProductsDto) {
    const newProducts: Products = {
      id: lastProductsID++,
      ...createProductsDto,
    };
    products.push(newProducts);
    return newProducts;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((products) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductsDto: UpdateProductsDto) {
    const index = products.findIndex((products) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProducts: Products = {
      ...products[index],
      ...updateProductsDto,
    };
    products[index] = updateProducts;
    return updateProducts;
  }

  remove(id: number) {
    const index = products.findIndex((products) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteProducts = products[index];
    products.splice(index, 1);
    return deleteProducts;
  }

  reset() {
    products = [
      { id: 1, name: 'cake', price: 60 },
      { id: 2, name: 'candy', price: 10 },
      { id: 3, name: 'bread', price: 40 },
    ];
    lastProductsID = 4;
    return 'RESET';
  }
}
